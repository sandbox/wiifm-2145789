<?php

/**
 * Simple AJAX callback that could interact with the database in some fashion.
 * In this really example we look up a node's title.
 */
function js_example_ajax_results() {
  $nid = arg(3);
  $title = db_query('SELECT title FROM {node} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
  if (empty($title)) {
    drupal_json_output(array(
      'title' => '',
      'success' => FALSE,
    ));
  }
  else {
    drupal_json_output(array(
      'title' => $title,
      'success' => TRUE,
    ));
  }

  drupal_exit();
}

/**
 * Hook_menu() response implementation.
 */
function js_example_menu_results($node) {
  drupal_json_output(array(
    'title' => $node->title,
    'success' => TRUE,
  ));
  drupal_exit();
}
